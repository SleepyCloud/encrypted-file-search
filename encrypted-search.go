package main

import (
    "fmt"
    "time"
    "bytes"
    "crypto/aes"
    "crypto/cipher"
    "strings"
    "crypto/sha1"
    "io"
    "os"
)

/**
prepares the search term by encrypting it with our block 
cipher and then hash it to create the word specific key and return
both
*/
func prep_search(term string, key string) []byte {
    padded_term := pad(term)//pad our search term to encrypt

    encrypted_word := encrypt_word([]byte(padded_term))// encrypt the word
    word_key := hash_words(string(encrypted_word[:10]), key) //hash the left of the encrypted word with our key

    return append(encrypted_word, word_key...)//append the encrypted word and word key and return
}

/**
xor two words and return the result as a byte array
*/
func xor_words(first, second []byte) []byte {
    n := len(first)
    b := make([]byte, n)
    for i := 0; i < n; i++ {
        b[i] = first[i] ^ second[i]
    }
    return b
}

/**
takes a a word and pads it to 16 bytes long using non-printing
characters so we know what to trim off
*/
func pad(word string) string {
    size := len(word)

    if size < 16 {
        return strings.Repeat("\x14", 16-size) + word
    }else if size > 16 {
        panic("Word is greater than 16 bytes")
    }
    return word
}

/**
takes two strings and hashes them together
*/
func hash_words(first, second string) []byte {
    h := sha1.New()
    s:= []string {first, second}//join needs a slice
    io.WriteString(h,strings.Join(s,""))

    return h.Sum(nil)
}

/**
encrypt the word using a block cipher
*/
func encrypt_word(word []byte) []byte {
    key := []byte("0123456789ABCDEF")
    if len(word)%aes.BlockSize != 0 {
        panic("Plaintext is not a multiple of the block size")
    }
    block, err := aes.NewCipher(key)//create the cipher
    if err != nil {
        panic(err)//report any errors creating it
    }

    ciphertext := make([]byte, aes.BlockSize)
    iv := []byte("this is an iv123")

    mode := cipher.NewCBCEncrypter(block, iv)
    mode.CryptBlocks(ciphertext[:aes.BlockSize], word)
    return ciphertext

}

/**
searches an encrypted file using the scheme 
described by song et al
*/
func search_file(filename string, query []byte) bool {
    term:= query[:16]//the first chunk is the search term
    word_key:= query[16:]//the second is the word specific key

    f, err := os.Open(filename)//open the file

    if err != nil {
        panic(err)
    }
    current_block:= make([]byte, 16)//buffer for each block
    for {
        n,err :=f.Read(current_block)//read the block
        if err != nil && err != io.EOF {
            panic(err)//report any errors
        }
        if n == 0 {//if we didn't read any bytes we're done
            break
        }
        key := xor_words(term, current_block)//get the key used to encrypt
        s_i := key[:10]//get the random numbers used to encrypt

        current_key :=hash_words(string(s_i), string(word_key))[:6]//gen a word specific key

        if bytes.Equal(current_key, key[10:]){//check if it's correct
            return true
        }
    }
    return false
}

func main() {
    start := time.Now()
    fmt.Printf("%b\n", search_file("Nintendo_DS.txt",prep_search("rabble", "this is a key")))
    elapsed := time.Since(start)
    fmt.Printf("All done in %s\n", elapsed)
}
